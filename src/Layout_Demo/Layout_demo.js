import React, { Component } from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";

export default class Layout_demo extends Component {
  render() {
    return (
      <div>
        <Header></Header>
        <div className="container row mx-auto">
          <div className="col-lg-4">
            <Content></Content>
          </div>
          <div className="col-lg-4">
            <Content></Content>
          </div>
          <div className="col-lg-4">
            <Content></Content>
          </div>
          <div className="col-lg-4">
            <Content></Content>
          </div>
          <div className="col-lg-4">
            <Content></Content>
          </div>
          <div className="col-lg-4">
            <Content></Content>
          </div>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}
